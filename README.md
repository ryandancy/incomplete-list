# An Incomplete List

A simple webpage, made as a media representation of Chapter 6 of *Station Eleven* by Emily St. John Mandel.

Available at http://coppercoder.gitlab.io/incomplete-list.

For best results, don't resize your screen while viewing the webpage. If I didn't have chemistry this semester I might have fixed that, but as of yet I am rather swamped.
